//Um die Scanner-Klasse verwenden zu können,
//muss sie aus dem util-Package importiert werden.
import java.util.Scanner;

public class ScannerDemo {
	public static void main(String[] args) {
		
		//Erzeugen eines Objekts der Klasse Scanner
		Scanner myScanner = new Scanner(System.in);
			
		System.out.println("Bitte geben Sie eine ganze Zahl ein: ");
		//Einlesen von der Konsole
		int i = myScanner.nextInt();
		//Ausgabe der Zahl
		System.out.println("Sie haben " + i + " eingegeben!");
			
		System.out.println("Bitte geben Sie eine Zahl mit Nachkommastellen ein: ");
		//Einlesen von der Konsole
		float f = myScanner.nextFloat();
		//Ausgabe der Zahl
		System.out.println("Sie haben " + f + " eingegeben!");
		
		System.out.println("Bitte geben Sie eine beliebige Zeichenkette ein: ");
		//Einlesen von der Konsole
		String s = myScanner.next();
		//Ausgabe der Zeichenkette
		System.out.println("Sie haben '" + s + "' eingegeben!");
		
		System.out.println("Bitte geben Sie einen Wahrheitswert ein (true / false): ");
		//Einlesen von der Konsole
		boolean b = myScanner.nextBoolean();
		//Ausgabe der Zeichenkette
		System.out.println("Sie haben '" + b + "' eingegeben!");
	
		System.out.println("Bitte geben Sie einen einzelnen Buchstaben ein: ");
		//Einlesen von der Konsole (Die Klasse Scanner kennt kein nextChar())
		char c = myScanner.next().charAt(0);
		//Ausgabe der Zeichenkette
		System.out.println("Sie haben '" + c + "' eingegeben!");
	}
}
