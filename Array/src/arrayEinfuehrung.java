

	/**
	  *
	  * Beschreibung
	  *  Einf�hrung in die Programmierung mit Arrays 
	  * @version 1.0 vom 19.03.2017
	  * @author 
	  */
	import java.util.Scanner;
	public class arrayEinfuehrung {
	  
	  public static void main(String[] args) {
	    
	    Scanner sc = new Scanner (System.in);
	    
	    //Deklaration eines Arrays, 5 integer Werte
	    //Name des Arrays: "intArray"
	    int[] intArray=new int[5];
	    
	    
	    
	    //Speichern von Werten im Array
	    //Wert 1000 an 3. Stelle des Arrays
	    //Wert 500 an Index 4
	    
	    intArray[2]=1000;
	    intArray[4]=500;
	    /*
	    System.out.println("Index 0: "+ intArray[0]);
	    System.out.println("Index 0: "+ intArray[1]);
	    System.out.println("Index 0: "+ intArray[2]);
	    System.out.println("Index 0: "+ intArray[3]);
	    System.out.println("Index 0: "+ intArray[4]);
	    System.out.println("Index 0: "+ intArray[5]);
	    */
	    //Array ausgaben mit For-Schleife
	    for (int i = 0; i < intArray.length; i++) {
	    	//Index an der stelle i (intArray [i])
	    System.out.println("Index" + i + ": " + intArray[i]);
	    }
	    
	    /*
	    for (int i = 0; i < 5; i++) {
	     System.out.println(intArray[i]); 
	    }
	    */
	    //Deklaration + Initialisierung eines Arrays in einem Schritt
	    //Name des Arrays: "doubleArray"
	    // L�nge 3, mit 3 double Werten 
	    //double[] doubleArray ={1.1,2.2,3.3};  
	    
	    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
	    //System.out.println(doubleArray[1]);
	    
	    
	    //Nutzer soll einen Index angeben 
	    //und an diesen Index (vom intArray) einen neuen Wert speichern
	    /*
	    System.out.println("An welchen Index soll der neue Wert?");
	    int inputIndex = sc.nextInt();
	    System.out.println("Geben Sie den neuen Wert f�r Index " + inputIndex + " an:");
	    intArray[inputIndex] = sc.nextInt();
	    System.out.println(intArray[inputIndex]);
	    */
	    
	    System.out.println("An welchen Index soll der neue Wert ?");
	    int index = sc.nextInt();
	    
	    System.out.println("Geben Sie den neuen Wert f�r index" + index + "an:");
	    int wert = sc.nextInt();
	    intArray[index] = wert;
	    
	    //Alle Werte vom Array intArray sollen ausgegeben werden
	    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
	    // 0   0  1000  0  500
	    /*
	    for (int i = 0; i < intArray.length; i++) {
	     System.out.printf("%d = %d\n",i,intArray[i]); 
	    }
	    */
	    
	    
	    //Der intArray soll mit neuen Werten gef�llt werden
	    //1. alle Felder sollen den Wert 0 erhalten
	    for (int i = 0; i < intArray.length; i++) {
	    	intArray[i] = 0;
	    }
	    for (int i = 0; i < intArray.length; i++) {
	    	System.out.println("Index " + i + ": "+ intArray[i]);
	    }
	    
	    
	    
	    //Der intArray soll mit neuen Werten gef�llt werden
	          //2. alle Felder sollen vom Nutzer einen Wert bekommen
	          //nutzen Sie dieses Mal die Funktion �length�
	    
	    
	    
	    //Der intArray soll mit neuen Werten gef�llt werden
	           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
	    int x = 10;
	    for (int i = 0; i < intArray.length; i++) {
	    	intArray[i] = x;
	    	x = x+10;
	    }
	    for (int i = 0; i < intArray.length; i++) {
	    	System.out.println("Index " + i + ": "+ intArray[i]);
	    }
	    
	    
	    
	    
	    
	  } // end of main
	  
	// end of class arrayEinfuehrungLoesung
	  }

