
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "Hello World";
		String s = "Java-Programm";
		String pfeil = "*************";
		int alter = 20;
		String name = "Henri";
		
		// "Hallo Welt" println= ausgeben und einen Zeilenvorschub ausf�hren
		// "Hallo Welt" \n= ausgeben und einen Zeilenvorschub ausf�hren
		System.out.print("Hello World \n");
		System.out.println("Hello World2");
		
		System.out.println(text);
		System.out.println(text);
	
		//Test von S�tzen und Escape_symbolen
		// "Text" print= nur ausgeben ohne Zeilenvorschub
		System.out.print("das ist ein satz.");
		System.out.print(" das ist noch ein satz.\n");
		System.out.println("Das ist ein Test.\"Ein Test ist das\"");
		
		// World = s ohne string
		System.out.printf("Hello %s!%n", "World");
		
		//befehle �ber string und int(integer)
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahrealt.");
		
		//pfeil erstellt
		System.out.printf( "|%5.1s|\n", pfeil );
		System.out.printf( "|%6.3s|\n", pfeil );
		System.out.printf( "|%7.5s|\n", pfeil );
		System.out.printf( "|%8.7s|\n", pfeil );
		System.out.printf( "|%9.9s|\n", pfeil );
		System.out.printf( "|%10.11s|\n", pfeil );
		System.out.printf( "|%11.12s|\n", pfeil );
		System.out.printf( "|%6.3s|\n", pfeil );
		System.out.printf( "|%6.3s|\n", pfeil );
		
		System.out.print("Das ist ein Test.");
		
}
	}

