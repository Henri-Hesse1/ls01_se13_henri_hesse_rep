import java.util.Scanner;

public class methoden_uebung_test {
	static Scanner myScanner = new Scanner(System.in);
		
		public static void main(String[] args) {
			
			  artikel();
			
			  double nettopreis = nettogesamtpreis (anzahl(),preis()) ;
			  bruttogesamtpreis (nettopreis,mwst());
		
			   
			   myScanner.close();
			
		}
			
			
			// Benutzereingaben lesen

		public static void artikel () {	
			System.out.println("was m�chten Sie bestellen?");
			String artikel = myScanner.next();
		return ;
			}
			
		public static double anzahl() {	
			System.out.println("Geben Sie die Anzahl ein:");
			int anzahl = myScanner.nextInt();
		return anzahl ;
			}

		public static double preis() {	
			System.out.println("Geben Sie den Nettopreis ein:");
			double preis = myScanner.nextDouble();
		return preis ;
			}

		public static double mwst() {	
			System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			double mwst = myScanner.nextDouble();
		return mwst;
			}
			
			
			
			
			// Verarbeiten
		public static double nettogesamtpreis(double anzahl, double preis) {
				
			double nettogesamtpreis = anzahl * preis;
			
		return nettogesamtpreis;
			}
		
		public static double bruttogesamtpreis(double nettogesamtpreis, double mwst) {
				
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			
		return bruttogesamtpreis;
			}
			
			
			
		public static void Ausgabe(String artikel, double anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			// Ausgeben

			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
			
			return;
			}
}

