package operatoren;

public class logischeoperatoren {

	public static void main(String[] args) {
		 /* 1. Deklarieren Sie zwei Warheitswerte a und b.*/
		  boolean a;
		  boolean b;
	    
	    /* 2. Initialisieren Sie einen Wert mit true, den anderen mit false */
		  a = true;
		  b = false;
	    
	    /* 3. Geben Sie beide Werte aus */
	    System.out.println(a);
	    System.out.println(b);
		  
	    /* 4. Deklarieren Sie einen Wahrheitswert undGatter */
	    
	    boolean undGatter;
	    
	    /* 5. Weisen Sie der Variable undGatter den Wert "a AND b" zu 
	          und geben Sie das Ergebnis aus. */
	    
	    undGatter = a && b;
	    
	    System.out.println(undGatter);
	          
	    /* 6. Deklarieren Sie au�erdem den Wahrheitswert c und initialisieren ihn
	          direkt mit dem Wert true */
	    boolean c = true;
	      
	    /* 7. Verkn�pfen Sie alle drei Wahrheitswerte a, b und c und geben Sie
	          jeweils das Ergebnis aus */
	       // a)  a AND b AND c
	       // b)  a OR  b OR  c
	       // c)  a AND b OR  c
	       // d)  a OR  b AND c
	       // e)  a XOR b AND c
	       // f) (a XOR b) OR c
	    System.out.println("a AND b AND c: " + (a && b && c));   
	    System.out.println("a OR  b OR  c: " + (a || b || c));   
	    System.out.println("a AND b OR: " + (a && b || c));   
	    System.out.println("a AND b AND c: " + (a && b && c));   
	    System.out.println("a XOR b AND c: " + (a ^ b && c));   
	    System.out.println("(a XOR b) OR c: " + ((a ^ b) || c));   
	    
	    
	   // end of main

	 // end of class LogischeOperatoren


	}

}
