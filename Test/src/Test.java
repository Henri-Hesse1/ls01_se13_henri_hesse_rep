
public class Test {

	public static void main(String[] args) {
		/*
		 * Dies ist ein mehrzeiliger 
		 * Kommentar
		 */
		
		
		//Arbeit mit Variablen
		//Deklaration einer Ganzzahligen Variablen "zahl1"
		//Initialisierung mit dem Wert 5
		int zahl1 = 5;
		int zahl2 = 18;
		int ergebnis;
		String ausgabe = "F�nf plus achtzehn ist gleich ";
		ergebnis = zahl1+zahl2;
		System.out.println(ausgabe+ergebnis);
		System.out.println(zahl1);
		System.out.println("Der Wert der Variablen ist: "+zahl1);
		System.out.println(zahl1+zahl2);
		System.out.println(zahl1+" 456    "+zahl2);
		
		//print erzeugt Ausgabe ohne Zeilenumbruch 
		System.out.print("Heute ist leider Montag\n");
		//println Ausgabe mit Zeilenumbruch 
		System.out.println("seit froh");

	}

}
