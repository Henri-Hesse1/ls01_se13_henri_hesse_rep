﻿import java.util.Scanner;

class Fahrkartenautomat
{
static double ticketPreis;
static int anzahlTickets;
static double zuZahlenderBetrag; 
static double eingezahlterGesamtbetrag;
static double eingeworfeneMünze;
static double rückgabebetrag;
static String euro = "Euro";
static String ausgabe;

static Scanner tastatur = new Scanner(System.in);


public static void main(String[] args)
{
   
  zuZahlenderBetrag = fahrkartenbestellungErfassen();
  eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
  fahrkartenAusgeben();
  rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
  
   
   tastatur.close();
}




public static double fahrkartenbestellungErfassen() {
  
  System.out.print("Ticketpreis (Euro): ");
    ticketPreis = tastatur.nextDouble();
    
    System.out.print("Anzahl der Tickets: ");
    anzahlTickets = tastatur.nextInt();
    
    
    zuZahlenderBetrag = ticketPreis*anzahlTickets;
  
    return zuZahlenderBetrag;
}

public static double fahrkartenBezahlen(double a) {
  
    eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < a)
    {
     System.out.format("Noch zu zahlen: %.2f %s\n",(a - eingezahlterGesamtbetrag), euro);
     System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     eingeworfeneMünze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneMünze;
    }
    return eingezahlterGesamtbetrag;
}

public static void fahrkartenAusgeben() {
  
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 20; i++)
    {
       System.out.print("=");
       try {
  Thread.sleep(50);
} catch (InterruptedException e) {
  e.printStackTrace();
}
    }
    System.out.println("\n\n");
}

public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

    rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if(rückgabebetrag > 0.0)
    {
     System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
        System.out.println("2 EURO");
        rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
        System.out.println("1 EURO");
        rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
        System.out.println("50 CENT");
        rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
        System.out.println("20 CENT");
          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
        System.out.println("10 CENT");
        rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
        System.out.println("5 CENT");
          rückgabebetrag -= 0.05;
        }
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    
/*
Aufgabe 5:
Der Ticketpreis hat den Datentyp double bekommen, da dieser der genauste bei den Gleitkomma-Datentypen ist und groß genug.


Die Variable anzahlTickets hat Integer bekommen, da man nur ganzzahlige Ticket-Anzahlen bekommen kann.
Es wird niemand 2,1 Milliarden Tickets kaufen, aber es ist möglich, long sollte nicht nötig sein.



Aufgabe 6:
Bei anzahl*einzelpreis wird der Wert der ganzzahligen Variable "anzahl" mit dem Wert der Gleitkomma Variable multipliziert.
Das Ergebnis ist somit eine Komma-Zahl.
*/
    }
}